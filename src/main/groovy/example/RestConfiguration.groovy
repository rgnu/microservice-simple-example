package example


import org.apache.camel.builder.RouteBuilder
import org.apache.camel.model.rest.RestBindingMode
import org.apache.camel.model.rest.RestParamType

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class RestConfiguration extends RouteBuilder {

	@Value('${rest.host}')      String host
	@Value('${rest.port}')      String port
	@Value('${rest.component}') String component

	@Override
	void configure() throws Exception {
		restConfiguration()
			.component(component)
			.host(host)
			.port(port)
			.bindingMode(RestBindingMode.auto)
	}
}
