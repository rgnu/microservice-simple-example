package example.controller

import org.apache.camel.ProducerTemplate
import org.apache.camel.Body

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import example.model.HealthCheckModel

@Component('HealthCheckController')
class HealthCheckController {

  @Autowired
  ProducerTemplate producerTemplate

  HealthCheckModel handler(@Body String body) {
    producerTemplate.sendBody("log:out", body)
    return [status: 'ok'] as HealthCheckModel
  }
}
