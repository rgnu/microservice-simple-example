package example.impl

import java.util.Collection
import java.util.Map
import java.util.TreeMap

import org.springframework.stereotype.Service

import example.api.ThingService
import example.model.Thing


@Service("thingService")
class ThingServiceMemoryImpl implements ThingService {

    private final Map<Integer, Thing> things = new TreeMap<Integer, Thing>();


    @Override
    public Thing findById(Integer id) {
        return things.get(id);
    }

    @Override
    public Collection<Thing> findAll() {
        return things.values();
    }

    @Override
    public void update(Thing thing) {
        things.put(thing.id, thing);
    }

}
