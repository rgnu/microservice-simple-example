package example.api

import java.util.Collection

import example.model.Thing

/**
 * Service interface for managing users.
 *
 */
public interface ThingService {

    /**
     * Find a user by the given ID
     *
     * @param id
     *            the ID of the user
     * @return the user, or <code>null</code> if user not found.
     */
    Thing findById(Integer id);

    /**
     * Find all users
     *
     * @return a collection of all users
     */
    Collection<Thing> findAll();

    /**
     * Update the given user
     *
     * @param user
     *            the user
     */
    void update(Thing thing);

}
