package example


import org.apache.camel.builder.RouteBuilder

import org.springframework.stereotype.Component


@Component
class ThingRoute extends RouteBuilder
{
	@Override
	void configure() throws Exception
	{

		from('direct:createThing').routeId('ThingsModel::Create')
			.to('jpa:au.com.sixtree.blog.Thing')

		from('direct:getThing').routeId('ThingsModel::Get')
			.to('sql:select * from THING where id = :#${header.id}?dataSource=dataSource&outputType=SelectOne')
			.beanRef('transformer', 'mapThing')

		from('direct:getThings').routeId('ThingsModel::List')
			.setProperty('query').method('transformer', 'constructQuery(${headers})')
			.toD('sql:${property.query}?dataSource=dataSource')
			.beanRef('transformer', 'mapThingSearchResults')
			.to('log:ThingsModel::List')

		from('direct:removeThing').routeId('ThingsModel::Remove')
			.to('direct:getThing')
			.setProperty('thing', body())
			.to('sql:delete from THING where id = :#${body.id}?dataSource=dataSource')
			.setBody(property('thing'))

	}
}
