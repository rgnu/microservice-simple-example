package example

import org.springframework.stereotype.Component

import example.model.Thing
import example.model.ThingSearchResults


@Component('transformer')
class Transformer {
    Thing mapThing(Map map) {
        new Thing(id: map.id, name: map.name, owner: map.owner)
    }

    String constructQuery(Map headers) {
        def wheres = []
        if (headers.name) {
            wheres << 'name = :#${header.name}'
        }
        if (headers.owner) {
            wheres << 'owner = :#${header.owner}'
        }

        def query = 'select * from THING'
        if (wheres) {
            query += ' where ' + wheres.join(' and ')
        }
        return query
    }

    ThingSearchResults mapThingSearchResults(List<Map> body) {
        new ThingSearchResults(
                size: body.size,
                things: body.collect { mapThing it }
        )
    }
}
