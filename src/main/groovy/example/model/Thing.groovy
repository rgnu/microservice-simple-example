package example.model

import javax.persistence.*

@Entity(name = "THING")
class Thing {
    @Id @GeneratedValue @Column(name = "ID") Integer id
    @Column(name = "NAME") String name
    @Column(name = "OWNER") String owner
}
