package example.model

class ThingSearchResults {
    Integer size
    List<Thing> things
}
