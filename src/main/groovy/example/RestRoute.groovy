package example


import org.apache.camel.builder.RouteBuilder

import org.springframework.stereotype.Component

import example.model.Thing
import example.model.ThingSearchResults
import example.model.HealthCheckModel


@Component
class RestRoute extends RouteBuilder
{
	@Override
	void configure() throws Exception
	{
		rest('/things')
			.post()
				.type(Thing)
				.to('direct:createThing')
			.get()
				.outType(ThingSearchResults)
				.to('direct:getThings')
			.get('/{id}')
				.outType(Thing)
				.to('direct:getThing')
			.delete('/{id}')
				.outType(Thing)
				.to('direct:removeThing')

		rest('/health')
			.get()
				.produces('application/vnd.health+json')
				.outType(HealthCheckModel)
			.route().routeId('rest::HealthCheck')
				.to('bean:HealthCheckController')
	}
}
