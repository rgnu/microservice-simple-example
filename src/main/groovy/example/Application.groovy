package example

import org.apache.camel.builder.RouteBuilder
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication


@SpringBootApplication
class Application {
	static void main(String[] args) {
		SpringApplication.run Application, args
	}
}
